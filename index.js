let http = require("http");

http.createServer(function (request, response) {
	if (request.url == "/" && request.method == "GET") {
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.write("Welcome to Booking System ( •̀ᴗ•́ )و ");
		response.end();
	}
	if (request.url == "/profile" && request.method == "GET") {
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.write("Welcome to your profile! ٩(^ᴗ^)۶");
		response.end();
	}
	if (request.url == "/courses" && request.method == "GET") {
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.write("Here’s our courses available (ó﹏ò｡)");
		response.end();
	}
	if (request.url == "/addCourse" && request.method == "POST") {
		response.writeHead(200,{'Content-Type':'application/json'});
		response.write("Add a course to our resources ༼ ༎ຶ ෴ ༎ຶ ༽");
		response.end();
	}
	if (request.url == "/updateCourse" && request.method == "PUT") {
		response.writeHead(200,{'Content-Type':'application/json'});
		response.write("Update a course to our resources ( ＾◡＾)っ✂❤");
		response.end();
	}
	if (request.url == "/achriveCourses" && request.method == "DELETE") {
		response.writeHead(200,{'Content-Type':'application/json'});
		response.write("Archive courses to our resources ▽д▽）");
		response.end();
	}
}).listen(4000);